How to change the boot logo on newer Lenovo ThinkPad notebooks
Require each iso file separately copied onto a USB stick after the iso file has been
trimmed with the geteltorito perl script. There seems to be an effort done to change
the original Lenovo ISO images so that the same approach as for the older models could
be used in the future, but is unfortunately not working yet.
Prerequisites

*  USB Stick
*  BIOS iso for your particular laptop model from https://support.lenovo.com/nl/en
*  Identifying ThinkPad X1 Carbon Type might come handy https://support.lenovo.com/nl/en/solutions/x1-carbon-help
*  geteltorito  bootimage extractor
*  root access to your Notebook
*  newer BIOS versions can only be installed with UEFI boot. Make sure you set your notebook temporary to UEFI before trying to update the BIOS.

Resolution

*  download the BIOS Update Bootable CD iso image from Lenovo Support web page
*  run the downloaded ISO file through the geteltorito.pl script
      `# perl geteltorito.pl -o bios_update_prep.iso bios_update.iso`
*  then  copy the prep iso file to your USB stick
      `# dd status=progress bs=4M if=bios_update_prep.iso of=/dev/sdX`
*  remove the USB stick and re-plug it again to the notebook
*  copy the logo to the "FLASH" folder on the USB stick

T480s, X1 Carbon 6th Gen
*     The logo should be as follow:
      -  width <= 1000
      -  height <= 500
      -  fileformat: *.gif (I exported a png in gimp as *.gif)
      -  no need to play around with color indexes or anything else

T460p
*     The logo should be as follow:
      -    filename: LOGO1.GIF
      -    width <= 640
      -    height <= 480
      -    fileformat: *.gif (I exported a png in gimp as *.gif)
      -    no need to play around with color indexes or anything else

Now booting from the USB stick should show the menu for the BIOS upgrade.
Select "update the system" in the menu to start the BIOS update. When asked if the custom logo should be used, answer with "Yes"